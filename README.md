# Order book app

## How to run the project

git clone https://gabrielguita@bitbucket.org/gabrielguita/order-book-app.git

- go to /order-book-app
- run "yarn install"
- run "yarn start"
- the project should run on http://localhost:3000/