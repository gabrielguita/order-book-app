/* eslint-disable react/no-danger */
import React from "react";
import PropTypes from "prop-types";
import {
  ETH_USD,
  ETH_EUR,
  BTC_USD,
  BTC_EUR,
} from "../../constants/general.constants";
import {
  Title,
  Wrapper,
  PairSelector,
  OrderBookContainer,
  Column,
  Row,
  Price,
  Qty,
} from "./styles";

const pairs = [
  { value: ETH_USD, label: ETH_USD },
  { value: ETH_EUR, label: ETH_EUR },
  { value: BTC_USD, label: BTC_USD },
  { value: BTC_EUR, label: BTC_EUR },
];

const OrderBookComponent = ({ handleOnChange, pair, buy, sell, curPrice }) => (
  <Wrapper>
    <Title>Active pair: {pair}</Title>
    <PairSelector onChange={(e) => handleOnChange(e)}>
      {pairs.map(({ value, label }) => (
        <option key={value} defaultValue={pair} value={value}>
          {label}
        </option>
      ))}
    </PairSelector>
    <OrderBookContainer>
      <Column>
        <Row className="head">
          <Price>Price({pair?.slice(4)})</Price>
          <Qty>Quantity({pair?.slice(4)})</Qty>
        </Row>
      </Column>

      <Column>
        {buy.map((item, i) => (
          <Row key={`${i}-buy`}>
            <Price className="buy">{item[1]}</Price>
            <Qty>{item[2]}</Qty>
          </Row>
        ))}
      </Column>

      <Column>
        <Row className="curPrice">{curPrice}</Row>
      </Column>
      <Column>
        {sell.map((item, i) => (
          <Row key={`${i}-sell`}>
            <Price className="sell">{item[1]}</Price>
            <Qty>{item[2]}</Qty>
          </Row>
        ))}
      </Column>
    </OrderBookContainer>
  </Wrapper>
);

let BuySell = PropTypes.arrayOf(PropTypes.string);

OrderBookComponent.propTypes = {
  handleOnChange: PropTypes.func.isRequired,
  pair: PropTypes.string,
  buy: PropTypes.arrayOf(BuySell),
  sell: PropTypes.arrayOf(BuySell),
  curPrice: PropTypes.string,
};

OrderBookComponent.defaultProps = {
  pair: null,
  buy: [],
  sell: [],
  curPrice: null,
};

export default OrderBookComponent;
