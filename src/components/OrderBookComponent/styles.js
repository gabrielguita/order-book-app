import styled from "styled-components";

export const colors = {
  white: "#cfcfcf",
  black: "#171717",
  yellow: "#fff0b9",
  green: "#01be70",
  red: "#ff4242",
  lightGrey: "#31313147",
};

export const Wrapper = styled.div`
  justify-content: center;
  display: flex;
  flex-direction: column;
  max-width: 500px;
  margin: 50px auto;
  letter-spacing: 0.45px;
`;

export const PairSelector = styled.select`
  max-width: 250px;
  margin: 20px auto;
`;

export const Price = styled.div``;
export const Title = styled.h1`
  font-size: 24px;
  text-align: center;
  color: ${colors.white};
`;

export const Qty = styled.div`
  margin-left: auto;
  color: ${colors.white};
`;

export const OrderBookContainer = styled.div`
  display: flex;
  max-width: 500px;
  flex-direction: column;
`;

export const Column = styled.div`
  display: flex;
  width: 100%;
  flex-direction: column;
`;

export const Row = styled.div`
  color: #fff;
  padding: 5px 15px;
  flex-direction: row;
  display: flex;
  font-size: 14px;
  &:nth-child(2n) {
    background: ${colors.lightGrey};
  }

  .buy {
    color: ${colors.green};
  }

  .sell {
    color: ${colors.red};
  }

  &.curPrice {
    background: ${colors.black};
    color: ${colors.yellow};
    padding: 15px;
    font-size: 15px;
    font-weight: bold;
  }
`;
