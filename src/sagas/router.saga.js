import { all, takeLatest } from "redux-saga/effects";
import { GET_ORDER_BOOK_DATA } from "../containers/OrderBook/constants";
import getOrderBookSaga from "../containers/OrderBook/sagas";

export default function* rootSaga() {
  yield all([takeLatest(GET_ORDER_BOOK_DATA, getOrderBookSaga)]);
}
