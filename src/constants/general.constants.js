export const ETH_USD = "ETH-USD";
export const ETH_EUR = "ETH-EUR";
export const BTC_USD = "BTC-USD";
export const BTC_EUR = "BTC-EUR";

export const API_URL = "wss://ws-feed.exchange.coinbase.com";
