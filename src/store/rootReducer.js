import { combineReducers } from "@reduxjs/toolkit";
import { connectRouter } from "connected-react-router";
import { orderBookReducer } from "../containers/OrderBook/reducer";

const rootReducer = (history) =>
  combineReducers({
    orderBook: orderBookReducer,
    router: connectRouter(history),
  });

export default rootReducer;
