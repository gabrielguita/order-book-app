import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { getOrderBookData as getOrderBookDataActions } from "./actions";
import OrderBookComponent from "../../components/OrderBookComponent";
import makeSelectOrderBookData from "./selector";
import { API_URL } from "../../constants/general.constants";

const OrderBook = ({ getOrderBookData, orderBookData }) => {
  const [pair, setPair] = useState("ETH-USD");
  const [curPrice, setCurPrice] = useState("");
  const [buy, setBuy] = useState([]);
  const [sell, setSell] = useState([]);

  const { sellData, buyData } = orderBookData;
  const payload = {
    type: "subscribe",
    product_ids: [pair],
    channels: [
      "level2",
      "heartbeat",
      {
        name: "ticker",
        product_ids: [pair],
      },
    ],
  };

  const handleOnChange = ({ target }) => {
    setBuy([]);
    setSell([]);
    setPair(target?.value);
  };

  useEffect(() => {
    const ws = new WebSocket(API_URL);
    ws.onopen = () => {
      ws.send(JSON.stringify(payload));
    };
    ws.onmessage = function (event) {
      try {
        if (!event?.data) return null;
        const json = JSON.parse(event.data);
        if (json?.price) {
          setCurPrice(json.price);
        }

        if (json?.product_id === pair && json?.changes) {
          if (json.changes[0].includes("buy")) {
            setBuy([...buy, json.changes[0]]);
          } else {
            setSell([...sell, json.changes[0]]);
          }
        }
      } catch (e) {
        console.log("Something went wrong with the WebSocket call:", e);
      }
    };
    ws.onclose = () => {
      ws.close();
    };
    return () => ws.close();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [buy, sell]);

  useEffect(() => {
    getOrderBookData({
      sell: sell.sort((a, b) => (a[1] > curPrice ? 1 : -1)),
      buy: buy.sort((a, b) => (a[1] > b[1] ? 1 : -1)),
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [buy, sell]);

  return (
    <OrderBookComponent
      pair={pair}
      handleOnChange={handleOnChange}
      buy={buyData}
      sell={sellData}
      curPrice={curPrice}
    />
  );
};

OrderBook.propTypes = {
  getOrderBookData: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  orderBookData: makeSelectOrderBookData(),
});

const mapDispatchToProps = (dispatch) => ({
  getOrderBookData: (data) => dispatch(getOrderBookDataActions(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(OrderBook);
