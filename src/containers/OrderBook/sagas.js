import { put } from "redux-saga/effects";
import { getOrderBookDataSuccess, getOrderBookDataFailure } from "./actions";

function* getOrderBookSaga({ payload }) {
  try {
    yield put(getOrderBookDataSuccess(payload));
  } catch (err) {
    yield put(getOrderBookDataFailure(err));
  }
}

export default getOrderBookSaga;
