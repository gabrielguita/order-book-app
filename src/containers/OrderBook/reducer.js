import {
  GET_ORDER_BOOK_DATA,
  GET_ORDER_BOOK_DATA_SUCCESS,
  GET_ORDER_BOOK_DATA_FAILURE,
} from "./constants";

export const initialState = {
  bid: { buy: [], sell: [] },
  isLoading: false,
  error: null,
};

export const orderBookReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_ORDER_BOOK_DATA:
      return {
        ...state,
        isLoading: true,
      };
    case GET_ORDER_BOOK_DATA_SUCCESS:
      const bid = {
        buy:
          payload?.buy?.length > 15
            ? payload.buy.slice(0, 11)
            : [...payload.buy],
        sell:
          payload?.sell?.length > 15
            ? payload.sell.slice(0, 11)
            : [...payload.sell],
      };

      return {
        isLoading: false,
        bid,
      };
    case GET_ORDER_BOOK_DATA_FAILURE:
      return {
        ...state,
        error: payload?.message,
        isLoading: true,
      };
    default:
      return state;
  }
};
