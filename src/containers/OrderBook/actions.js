import {
  GET_ORDER_BOOK_DATA,
  GET_ORDER_BOOK_DATA_SUCCESS,
  GET_ORDER_BOOK_DATA_FAILURE,
} from "./constants";

export const getOrderBookData = (payload) => ({
  type: GET_ORDER_BOOK_DATA,
  payload,
});

export const getOrderBookDataSuccess = (result) => ({
  type: GET_ORDER_BOOK_DATA_SUCCESS,
  payload: result,
});

export const getOrderBookDataFailure = (error) => ({
  type: GET_ORDER_BOOK_DATA_FAILURE,
  payload: error,
});
