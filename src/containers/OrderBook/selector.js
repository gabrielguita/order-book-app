import { createSelector } from "reselect";

const selectOrderBook = (state) => state?.orderBook;

const makeSelectOrderBookData = () =>
  createSelector(selectOrderBook, (substate) => {
    if (!substate?.bid) return { sell: [], buy: [] };

    const { sell, buy } = substate.bid;

    return {
      sellData: sell.slice(0, 10),
      buyData: buy.slice(0, 10),
    };
  });

export default makeSelectOrderBookData;
