import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import OrderBook from "./containers/OrderBook";
import { HOME_PATH } from "./constants/routes.constants";

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route path={HOME_PATH} exact component={OrderBook} />
    </Switch>
  </BrowserRouter>
);

export default Routes;
